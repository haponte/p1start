package edu.uprm.cse.datastructures.cardealer;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarList;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/cars")
public class CarManager {

  //  private CarComparator carComp = new CarComparator();
    private CircularSortedDoublyLinkedList<Car> listaCarro = CarList.getInstance().getCarList();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Car[] getCars() {
        Car[] carArr = new Car[listaCarro.size()];
        for (int i = 0; i < listaCarro.size(); i++) {
            carArr[i] = listaCarro.get(i);
        }
        return carArr;
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Car getCar(@PathParam("id") long id){

        for (int i = 0; i < listaCarro.size(); i++){
            if(listaCarro.get(i).getCarId() == id){
                return listaCarro.get(i);
            }
        }
        throw new NotFoundException("Car" + id + " not found");

    }

    @POST
    @Path("/add")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addCar(Car car){
        listaCarro.add(car);
        return Response.status(201).build();
    }

    @PUT
    @Path("{id}/update")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateCar(Car car){
        long matchId = car.getCarId();
        for (int i = 0; i< listaCarro.size(); i++){
            if(listaCarro.get(i).getCarId() == car.getCarId()){
                listaCarro.remove(i);
                listaCarro.add(car);
                return Response.status(200).build();
            }
        }
        return Response.status(404).build();
    }

    @DELETE
    @Path("{id}/delete")
    public Response deleteCar(@PathParam("id")long id){
        for (int i = 0; i< listaCarro.size(); i++){
            if (listaCarro.get(i).getCarId() == id){
                listaCarro.remove(i);
                return Response.status(200).build();

            }
        }
        return Response.status(404).build();
    }





}
